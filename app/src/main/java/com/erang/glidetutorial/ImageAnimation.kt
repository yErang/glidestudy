package com.erang.glidetutorial

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import com.bumptech.glide.GenericTransitionOptions
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_image_animation.*

class ImageAnimation : AppCompatActivity() {

    lateinit var btnAniOn : Button
    lateinit var btnAniOff: Button
    lateinit var btnAniCustom : Button
    lateinit var ivTest : ImageView
    private val url = "http://cfs9.tistory.com/upload_control/download.blog?fhandle=YmxvZzE1NDExNkBmczkudGlzdG9yeS5jb206L2F0dGFjaC8wLzM0MDAwMDAwMDAwMi5qcGc%3D"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_animation)



        init()
        setClickListener()


    }

    fun init(){
        ivTest = animationiv
        btnAniOn = animationOn
        btnAniOff = animationOff
        btnAniCustom = animationCustom
    }

    fun setClickListener(){
        btnAniOn.setOnClickListener {
            ivTest.setImageResource(android.R.color.transparent)
            Glide.with(this)
                    .load(url)
                    .transition(DrawableTransitionOptions()
                            .crossFade(3000))
                    .apply(RequestOptions()
                            .placeholder(R.mipmap.ic_launcher)
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter())
                    .into(ivTest)
        }
        btnAniOff.setOnClickListener {
            ivTest.setImageResource(android.R.color.transparent)
            Glide.with(this)
                    .load(url)
                    .transition(DrawableTransitionOptions()
                            .dontTransition())
                    .apply(RequestOptions()
                            .placeholder(R.mipmap.ic_launcher)
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter())
                    .into(ivTest)
        }

        btnAniCustom.setOnClickListener{
            ivTest.setImageResource(android.R.color.transparent)
            Glide.with(this)
                    .load(url)
                    .transition(GenericTransitionOptions.with (R.anim.anim))
                    .apply(RequestOptions()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter())
                    .into(ivTest)
        }

    }

}
