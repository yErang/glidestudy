package com.erang.glidetutorial

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    lateinit var btnBasic : Button
    lateinit var btnAnimation : Button
    lateinit var btnTransformation: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        setClickListener()



    }

    fun init(){
        btnBasic = basicPage
        btnAnimation = animationPage
        btnTransformation = transformationPage
    }

    fun setClickListener(){
        btnBasic.setOnClickListener {
            val intentBasic = Intent(this@MainActivity,ImageLoading::class.java)
            startActivity(intentBasic)
        }
        btnAnimation.setOnClickListener {
            val intentAnimation = Intent(this@MainActivity,ImageAnimation::class.java)
            startActivity(intentAnimation)
        }
        btnTransformation.setOnClickListener {
            val intentTransformation = Intent(this@MainActivity,ImageTransformation::class.java)
            startActivity(intentTransformation)
        }
    }

}
