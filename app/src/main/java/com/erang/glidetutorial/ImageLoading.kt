package com.erang.glidetutorial

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_image_loading.*

class ImageLoading : AppCompatActivity() {


    lateinit var btnBasic : Button
    lateinit var btnBasicWithPlaceholder : Button
    lateinit var btnBasicWithError : Button
    lateinit var btnBasicThumbnail : Button
    lateinit var btnBasicGIF : Button
    private val url = "http://cfs9.tistory.com/upload_control/download.blog?fhandle=YmxvZzE1NDExNkBmczkudGlzdG9yeS5jb206L2F0dGFjaC8wLzM0MDAwMDAwMDAwMi5qcGc%3D"
    private val gifurl = "https://techcrunch.com/wp-content/uploads/2015/08/safe_image.gif?w=1390&crop=1"

    lateinit var ivTest : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_loading)


        init()
        setClickListener()

        //기능테스트를 위해 이미지 캐싱 X



    }

    fun init(){
        ivTest = basiciv
        btnBasic = basic
        btnBasicWithPlaceholder = basic_placeholder
        btnBasicWithError = basic_error
        btnBasicThumbnail = basic_thumbnail
        btnBasicGIF = basic_GIF
    }

    fun setClickListener(){
        btnBasic.setOnClickListener{
            ivTest.setImageResource(android.R.color.transparent)

            Glide.with(applicationContext)
                    .load(url)
                    .apply(RequestOptions()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter())
                    .into(ivTest)
        }
        btnBasicWithPlaceholder.setOnClickListener {
            ivTest.setImageResource(android.R.color.transparent)

            Glide.with(applicationContext)
                    .load(url)
                    .apply(RequestOptions()
                            .placeholder(R.mipmap.ic_launcher)
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter())
                    .into(ivTest)
        }
        btnBasicWithError.setOnClickListener {
            ivTest.setImageResource(android.R.color.transparent)
            Glide.with(applicationContext)
                    .load("http://cfs9.tistory.com/upload_control/download.blog?fhandle=YmxvZzE1NDExNkBmczkudGlzdG9yeS5jb206L2F0dGFjaC8wLzM0MDAwMDAwMDAwMi5qcGc%")
                    .apply(RequestOptions()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter()
                            .error(R.mipmap.ic_launcher_round))
                    .into(ivTest)
        }
        btnBasicThumbnail.setOnClickListener {
            ivTest.setImageResource(android.R.color.transparent)
            Glide.with(applicationContext)
                    .load(url)
                    .apply(RequestOptions()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter()
                            .error(R.mipmap.ic_launcher_round))
                    .thumbnail(0.1f)
                    .into(ivTest)
        }
        btnBasicGIF.setOnClickListener {
            Glide.with(this)
                    .load(gifurl)
                    .thumbnail(0.1f)
                    .into(ivTest)
        }
    }
}
