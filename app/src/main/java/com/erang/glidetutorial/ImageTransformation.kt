package com.erang.glidetutorial

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_image_transformation.*

class ImageTransformation : AppCompatActivity() {

    lateinit var btnDefault :Button
    lateinit var btnCentercrop: Button
    lateinit var btnFitCenter: Button
    lateinit var btnCircle: Button
    lateinit var ivTransformation: ImageView
    private val url = "http://cfs9.tistory.com/upload_control/download.blog?fhandle=YmxvZzE1NDExNkBmczkudGlzdG9yeS5jb206L2F0dGFjaC8wLzM0MDAwMDAwMDAwMi5qcGc%3D"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_transformation)
        init()
        setClickListener()
    }


    fun init() {
        ivTransformation = transiv
        btnDefault = trans_default
        btnCentercrop = trans_centercrop
        btnFitCenter = trans_fitcenter
        btnCircle = trans_circle
    }

    fun setClickListener() {
        btnDefault.setOnClickListener {
            Glide.with(this)
                    .load(url)
                    .apply(RequestOptions()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                    )
                    .into(ivTransformation)
        }
        btnCentercrop.setOnClickListener {
            ivTransformation.setImageResource(android.R.color.transparent)
            Glide.with(this)
                    .load(url)
                    .apply(RequestOptions()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .centerCrop()
                    )
                    .into(ivTransformation)
        }
        btnFitCenter.setOnClickListener {
            ivTransformation.setImageResource(android.R.color.transparent)
            Glide.with(this)
                    .load(url)
                    .apply(RequestOptions()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter()
                    )
                    .into(ivTransformation)
        }
        btnCircle.setOnClickListener {
            ivTransformation.setImageResource(android.R.color.transparent)
            Glide.with(this)
                    .load(url)
                    .apply(RequestOptions()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .circleCrop()

                    )
                    .into(ivTransformation)
        }



    }
}
